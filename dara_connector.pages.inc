<?php
/**
 * @file
 *  Submit content to da|ra
 */


/**
 * dara_connector settings
 */
function dara_connector_admin_settings($form, &$form_state) {
  drupal_set_title(t('DARA connector settings'));
  $form['access_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Access Settings'),
    '#collapsible' => TRUE,
  );

  $form['access_settings']['dara_connector_access_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('dara_connector_access_username', ''),
  );

  $form['access_settings']['dara_connector_access_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('dara_connector_access_password', ''),
  );
  $form['connection_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Settings'),
    '#collapsible' => TRUE,
  );
  $form['connection_settings']['dara_connector_connection_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint URL'),
    '#default_value' => variable_get('dara_connector_connection_url', 'http://www.da-ra.de/dara/study/importXML'),
    '#description' => t('Normally this should be http://www.da-ra.de/dara/study/importXML') . '<br />' . t('For testing this should be http://dara-test.gesis.org:8084/dara/study/importXML'),
  );
  $form['sitespecific_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site-specific Settings'),
    '#collapsible' => TRUE,
  );

  $form['sitespecific_settings']['dara_connector_sitespecific_include_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of your site-specific include file'),
    '#field_suffix' => ' .sitespecific.inc',
    '#description' => t('This module requires a PHP include file that has been customized for your site. Please see example.sitespecific.inc file in the module directory.as an example and follow the directions given in that file. Allowed characters are 0-9, a-z, underscore and dash.'),
    '#default_value' => variable_get('dara_connector_sitespecific_include_name', ''),
  );
  $form['content_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Settings'),
    '#collapsible' => TRUE,
  );
  $node_types = node_type_get_names();
  $form['content_settings']['dara_connector_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled content types'),
    '#description' => t('Choose the types on which you would like to enable the dara connect functionality. Choose none to enable it on all content types.'),
    '#options' => $node_types,
    '#default_value' => variable_get('dara_connector_content_types', array()),
  );
  return system_settings_form($form);
}

/**
 * Get dara status of the given node
 * Return an array with the following keys
 * - update_necessary: TRUE/FALSE
 * - update_possible: TRUE/FALSE
 * - info_lines: array of HTML lines
 * - new_xml: XML that should be uploaded
 * - dara_xml: XML that was uploaded last time or ''
 * - dara_availability: availability as it was uploaded to dara last time, string or ''
 *   (values: '', 'Download', 'Delivery', 'On-site', 'Not available', 'Unknown'
 * - dara_doi: the registered doi or '' if not registered yet
 */
function _dara_connector_get_dara_status($node) {
  $lines=array();
  $update_necessary=FALSE;
  $update_possible=TRUE;

  // get dara info from the database, if any
  $dara_info=_dara_connector_load_dara_info($node->nid);

  // get dara XML for this node
  $new_xml = _dara_connector_get_dara_xml($node, $dara_info);

  // extract some interesting values from the old XML and from the new XML
  $old_values=array();
  $new_values=array();
  if ($new_xml) {
    $new_values=_dara_connector_get_dara_xml_values($new_xml);
  }
  if ($dara_info['dara_xml']) {
    $old_values=_dara_connector_get_dara_xml_values($dara_info['dara_xml']);
    if (empty($old_values['xml_valid']) || !empty($old_values['xml_errors'])) {
      // old XML files should have no problems
      $lines[]=t('Error: failed to validate the previously uploaded XML');
      $update_possible=FALSE;
    }
  }

  // add XML errors and warnings
  if (!empty($new_values['xml_errors'])) {
    foreach ($new_values['xml_errors'] as $msg) {
      $lines[]=$msg;
    }
  }
  if (empty($new_values['xml_valid'])) {
    $lines[]=t('Error: failed to generate a valid XML');
    $update_possible=FALSE;
  }
  else {
    // make sure that all values required for DOI identifications are there (and unchanged)
    $required_values=array('doi' => t('DOI'), 'identifier' => t('Identifier'), 'version' => t('Version'));
    foreach ($required_values as $key => $label) {
      if (!empty($new_values[$key])) {
        $lines[]= t('@label: @value', array('@label' => $label, '@value' => $new_values[$key]));
        if (!empty($old_values[$key]) && ($new_values[$key]!=$old_values[$key])) {
          $lines[]=t('Error: @label value must not be changed from old value: @value', array('@label' => $label, '@value' => $old_values[$key]));
          $upload_possible=FALSE;
        }
      }
      else {
        $lines[]=t('XML ERROR: @label is missing', array('@label' => $label));
        $update_possible=FALSE;
      }
    }
    // Check new availability
    if (empty($new_values['availability'])) {
      $lines[]=t('XML ERROR: @label is missing', array('@label' => t('new availability')));
      $update_possible=FALSE;
    }
    else  {
      // Check old availability
      $old_availability='';
      if (!empty($old_values['xml_valid'])) {
        if (empty($old_values['availability'])) {
          $old_availability=t('Old availability value is missing');
        }
        else {
          $old_availability=$old_values['availability'];
        }
      }
      else {
        $old_availability=t('Not uploaded yet');
      }

      // Compare old and new
      if ($new_values['availability']==$old_availability) {
        $lines[]=t('Availability: @availability', array('@availability' => $new_values['availability']));
      }
      else {
        $lines[]=t('Old availability: @availability', array('@availability' => $old_availability));
        $lines[]=t('New availability: @availability', array('@availability' => $new_values['availability']));
        $update_necessary=TRUE;
      }
    }
  }
  // make new and old XML downloadable
  if ($new_xml) {
    $lines[]=t('XML file to be uploaded: !link', array('!link' => l(t('dara_connector.xml'), 'node/' . $node->nid . '/dara_connector.xml')));
  }
  if (!empty($dara_info['dara_xml'])) {
    if ($new_xml!=$dara_info['dara_xml']) {
      $lines[]=t('XML file previously uploaded: !link (different)', array('!link' => l(t('dara_connector.old.xml'), 'node/' . $node->nid . '/dara_connector.old.xml')));
      $update_necessary=TRUE;
    }
  }

  // check whether last operation was successful
  if (!$update_necessary) {
    $result = db_query_range('SELECT nid, result_code FROM {dara_connector_node_log} WHERE nid=:nid ORDER BY upload_timestamp DESC', 0, 1, array(':nid' => $node->nid));
    foreach ($result as $record) {
      if (($record->result_code!=200) && ($record->result_code!=201)) {
        $lines[]=t('Last operation was not successful (Update necessary)');
        $update_necessary=TRUE;
      }
    }
  }
  return array(
    'update_necessary' => $update_necessary,
    'update_possible' => $update_possible,
    'info_lines' => $lines,
    'dara_doi' => $dara_info['dara_doi'],
    'dara_availability' => $dara_info['dara_availability'],
    'dara_xml' => $dara_info['dara_xml'],
    'new_xml' => $new_xml,
  );
}
/**
 * Build the form that uploads the content
 */
function dara_connector_page_connect_form($form, &$form_state, $node) {
  drupal_set_title(t('DOI'));
  $dara_status=_dara_connector_get_dara_status($node);
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  //
  // XML Information
  //
  $form['info'] = array(
    '#type' => 'fieldset',
    '#title' => t('XML Information'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['info']['lines'] = array(
    '#markup' => implode('<br />', $dara_status['info_lines']),
  );

  //
  // Update
  //
  $form['update'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  if ($dara_status['update_necessary']) {
    $form['update']['update_necessary'] = array(
      '#markup' => t('Update is necessary.') . '<br />',
    );
  }
  else {
    $form['update']['update_necessary'] = array(
      '#markup' => t('Update is not necessary.') . '<br />',
    );
  }
  if ($dara_status['update_possible']) {
    $form['update']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update information on da|ra'),
      '#submit' => array('dara_connector_page_connect_form_upload_submit'),
    );
    // Since the XML has been validated and displayed to the user, let's save it here
    // to make sure that the same information is uploaded that the user saw.
    $form['new_xml']=array(
      '#type' => 'value',
      '#value' => $dara_status['new_xml'],
    );
  }
  else {
    $form['update']['update_necessary'] = array(
      '#markup' => t('Update is not possible.') . '<br />',
    );
  }

  $header = array(t('Time'), t('User'), t('Availability'), t('Result'), t('Message'));
  $rows = array();
  $result = db_query('SELECT * FROM {dara_connector_node_log} WHERE nid=:nid ORDER BY upload_timestamp DESC', array(':nid' => $node->nid));
  foreach ($result as $record) {
    $username="";
    if ($account=user_load($record->upload_uid)) {
      $username=check_plain(format_username($account));
    }
    if ($username=="") {
      $username=t('User !uid', array('!uid' => $record->upload_uid));
    }
    $rows[] = array(
      format_date($record->upload_timestamp, 'short'),
      l($username, 'user/' . $record->upload_uid),
      check_plain($record->upload_availability),
      check_plain($record->result_code),
      check_plain($record->result_text),
    );
  }
  if (count($rows)) {
    $form["log"] = array(
      '#type' => 'fieldset',
      '#title' => t('Log'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['log']['table'] = array(
      '#markup' => check_plain('') . '<br />' . theme('table', array('header' => $header, 'rows' => $rows)) . '<br />',
    );
  }
  return $form;
}


/**
 * Button submit function to upload content
 */
function dara_connector_page_connect_form_upload_submit($form, &$form_state) {
  global $user;
  // get submitted values
  $nid=$form_state['values']['nid'];
  $new_xml=$form_state['values']['new_xml'];
  // extract doi and availability from the XML
  $new_values=_dara_connector_get_dara_xml_values($new_xml);

  $node=node_load($nid);
  if (!$node) {
    drupal_set_message(t('Failed to load node'), 'error');
    return;
  }
  // check whether the same XML is generated now as when the form was displayed to the user
  $dara_info=_dara_connector_load_dara_info($node->nid);
  $new_xml_again = _dara_connector_get_dara_xml($node, $dara_info);
  if ($new_xml!=$new_xml_again) {
    drupal_set_message(t('The XML content has changed between form load and form submit. Please reload the form and submit it again.'));
    return;
  }

  $username=variable_get('dara_connector_access_username', '');
  $password=variable_get('dara_connector_access_password', '');
  $url=variable_get('dara_connector_connection_url', 'http://www.da-ra.de/dara/study/importXML');
  if (($username=="") || ($password=="")) {
    drupal_set_message(t('Please configure username and password.'), 'error');
    return;
  }
  if ($url=="") {
    drupal_set_message(t('Please configure the Endpoint URL.'), 'error');
    return;
  }
  $url .= '?registration=true';
  // get dara info of the node from the database, if any
  $dara_info = _dara_connector_load_dara_info($node->nid);

  $headers=array();
  $headers['Authorization'] = 'Basic ' . base64_encode("$username:$password");
  $headers['Content-Type'] ='text/xml; charset=UTF-8';
  $options = array(
    'headers' => $headers,
    'timeout' => variable_get('http_request_timeout', 30),
    'method' => 'POST',
    'data' => $new_xml,
  );
  $result=drupal_http_request($url, $options);
  $result_code=0;
  $result_text="";
  if (is_object($result)) {
    if (isset($result->code)) {
      $result_code=$result->code;
    }
    if (isset($result->status_message)) {
      $result_text .= $result->status_message . ' ';
    }
    if (isset($result->error) && (!isset($result->status_message) || ($result->status_message!=$result->error))) {
      $result_text .= $result->error . ' ';
    }
    if (!empty($result->data)) {
      $result_text .= "DATA: $result->data";
    }
  }
  else {
    $result_text="Internal error";
  }
  // Add upload result to the log
  $id = db_insert('dara_connector_node_log')
  ->fields(array(
    'nid' => $node->nid,
    'upload_timestamp' => time(),
    'upload_uid' => $user->uid,
    'upload_availability' => $new_values['availability'],
    'result_code' => $result_code,
    'result_text' => $result_text,
  ))
  ->execute();

  // If upload was successful, save info
  if (($result_code==200) || ($result_code==201)) {
    $dara_info=array();
    $dara_info['nid']=$nid;
    $dara_info['dara_xml']=$new_xml;
    $dara_info['dara_doi']=$new_values['doi'];
    $dara_info['dara_availability']=$new_values['availability'];
    _dara_connector_save_dara_info($dara_info);
    drupal_set_message(t('Operation was successful.'));
  }
  else {
    drupal_set_message(t('Operation failed, please check the log below.'));
  }
}

/**
 * Show current status of the dara-enabled nodes
 * Note: SQL query could be improved when time permits,
 *     current implementation is VERY slow.
 */
function dara_connector_dara_status_page() {
  drupal_set_title(t('DOI status'));
  // check settings
  $username=variable_get('dara_connector_access_username', '');
  $password=variable_get('dara_connector_access_password', '');
  $url=variable_get('dara_connector_connection_url', 'http://www.da-ra.de/dara/study/importXML');
  if (($username=="") || ($password=="")) {
    drupal_set_message(t('Please configure username and password.'), 'error');
    return;
  }
  if (!_dara_connector_include_sitespecific()) {
    drupal_set_message(t('Please check the site-specific configuration.'), 'error');
  }
  // table for nodes that need update
  $header = array(t('Title'), t('DOI'), t('Availability'), t('Status'));
  $rows = array();
  // table for nodes that have been uploaded
  $header2 = array(t('Title'), t('DOI'),  t('Availability'), t('Status'));
  $rows2 = array();
  $output = '';

  // get configured content types
  $content_types=variable_get('dara_connector_content_types', array());
  if ($content_types) {
    // keep only the enabled content types in the array
    $content_types=array_filter($content_types);
  }

  // get all the nodes of the configured content types
  if ($content_types) {
    $result = db_query('SELECT nid, vid, title, type, status, changed FROM {node} WHERE type IN (:type) ORDER BY changed DESC', array(':type' => $content_types));
    $output .= t('Enabled content types: @list (!change)', array('@list' => implode(', ', $content_types), '!change' => l(t('change configuration'), 'admin/config/content/dara_connector')));
    $output .='<br />';
  }
  else {
    $result = db_query('SELECT nid, vid, title, type, status, changed FROM {node} ORDER BY changed DESC');
    $output .= t('Enabled content types: @list (!change)', array('@list' => t('All'), '!change' => l(t('change configuration'), 'admin/config/content/dara_connector')));
    $output .='<br />';
  }

  foreach ($result as $record) {
    $node=node_load($record->nid);
    if (!$node) {
      continue;
    }
    $dara_status=_dara_connector_get_dara_status($node);
    $todo=0; // 0=nothing to do, 1=something needs to be done
    if (!$dara_status['update_possible']) {
      $status=t('Error');
      $todo=1;
    }
    elseif (!$node->status && empty($dara_status['dara_doi'])) {
      $status=('Private');
      $todo=1;
    }
    elseif (!empty($dara_status['update_necessary']) && !empty($dara_status['dara_doi'])) {
      $status=t('Update');
      $todo=1;
    }
    elseif (!empty($dara_status['update_necessary']) && empty($dara_status['dara_doi'])) {
      $status=t('Upload');
      $todo=1;
    }
    else {
      $status=t('OK');
    }
    // title column
    $title = filter_xss(((mb_strlen($node->title)<50) ? $node->title : mb_substr($node->title, 0, 50) . '...'));
    if ($todo) {
      $rows[]=array(
        l($title, 'node/' . $record->nid),
        check_plain($dara_status['dara_doi']),
        check_plain($dara_status['dara_availability']),
        l($status, 'node/' . $record->nid . '/dara_connector'),
      );
    }
    else {
      $rows2[]=array(
        l($title, 'node/' . $record->nid),
        check_plain($dara_status['dara_doi']),
        check_plain($dara_status['dara_availability']),
        l($status, 'node/' . $record->nid . '/dara_connector'),
      );
    }
  }
  if (count($rows)) {
    $output .= theme('table', array('header' => $header, 'rows' => $rows, 'caption' => t('Nodes that need update')));
  }
  else {
    $output .= t('No update is necessary.');
  }
  if (count($rows2)) {
    $output .= theme('table', array('header' => $header2, 'rows' => $rows2, 'caption' => t('Uploaded nodes')));
  }
  return $output;

}

/**
 * Return an array with keys 'xml_valid' (TRUE/FALSE), 'xml_errors' (array with messages),
 *  and possibly 'identifier', 'version', 'doi', 'availability'.
 */
function _dara_connector_get_dara_xml_values($xml) {
  $xml_values=array(
    'xml_valid' => FALSE,
    'xml_errors' => array(),
  );
  if (!$xml || !is_string($xml)) {
    $xml_values['xml_errors'][]=t('XML ERROR: @message', array('@message' => t('invalid content')));
    return $xml_values;
  }
  try {
    libxml_use_internal_errors(true);
    libxml_clear_errors();
    $dom = new DOMDocument();
    if (!$dom->loadXML($xml)) {
      $unique_errors=array();
      $errors = libxml_get_errors();
      foreach ($errors as $error) {
        if (!empty($unique_errors[$error->message])) {
          continue;
        }
        switch ($error->level) {
          case LIBXML_ERR_WARNING:
            $xml_values['xml_errors'][]=t('XML WARNING: @message', array('@message' => $error->message));
            break;
          case LIBXML_ERR_ERROR:
            $xml_values['xml_errors'][]=t('XML ERROR: @message', array('@message' => $error->message));
            break;
          case LIBXML_ERR_FATAL:
          default:
            $xml_values['xml_errors'][]=t('XML ERROR: @message', array('@message' => $error->message));
            break;
        }
      }
      $unique_errors[$error->message]=TRUE;
      return $xml_values;
    }
  }
  catch (Exception $e) {
    $xml_values['errors'][]=t('XML Load error: @error', array('@error' => $e->getMessage()));
    return $xml_values;
  }
  $xml_values['xml_valid']=TRUE;
  // get values
  $resource_list = $dom->getElementsByTagName('resource');
  foreach ($resource_list as $resource_node) {
    $resourceidentifier_list = $resource_node->getElementsByTagName('resourceIdentifier');
    foreach ($resourceidentifier_list as $resourceidentifier_node) {
      $identifier_list = $resourceidentifier_node->getElementsByTagName('identifier');
      foreach ($identifier_list as $identifier_node) {
        $xml_values['identifier']=$identifier_node->nodeValue;
      }
      $version_list = $resourceidentifier_node->getElementsByTagName('currentVersion');
      foreach ($version_list as $version_node) {
        $xml_values['version']=$version_node->nodeValue;
      }
    }
    $doi_list = $resource_node->getElementsByTagName('doiProposal');
    foreach ($doi_list as $doi_node) {
      $xml_values['doi']=$doi_node->nodeValue;
    }
    $availability_list = $resource_node->getElementsByTagName('availability');
    foreach ($availability_list as $availability_node) {
      $availabilitytype_list = $availability_node->getElementsByTagName('availabilityType');
      foreach ($availabilitytype_list as $availabilitytype_node) {
        $xml_values['availability']=$availabilitytype_node->nodeValue;
      }
    }
  }
  return $xml_values;
}


/**
 * Show XML content on the /node/%node/dara_connector.xml page
 */
function dara_connector_page_show_xml($node) {
  if (_dara_connector_is_content_type_enabled($node->type)) {
    // get dara info from the database, if any
    $dara_info=_dara_connector_load_dara_info($node->nid);

    // get dara XML for this node
    $dara_xml = _dara_connector_get_dara_xml($node, $dara_info);

    if ($dara_xml) {
      drupal_add_http_header('Content-Type', 'text/xml');
      echo $dara_xml;
      drupal_exit();
    }
  }
  return t('Failed to get the XML');
}

/**
 * Show old XML content on the /node/%node/dara_connector.old.xml page
 */
function dara_connector_page_show_old_xml($node) {
  if (_dara_connector_is_content_type_enabled($node->type)) {
    // get dara info from the database, if any
    $dara_info=_dara_connector_load_dara_info($node->nid);
    if (!empty($dara_info['dara_xml'])) {
      drupal_add_http_header('Content-Type', 'text/xml');
      echo $dara_info['dara_xml'];
      drupal_exit();
    }
  }
  return t('Failed to get the XML');
}

/**
 * Return an array with possible availability values
 * - key=as expected by dara
 * - value=description of the key
 */
function _dara_connector_get_availability_values() {
  $values=array(
    'Download' => t('Released for everybody'),
    'Delivery' => t('Can be delivered'),
    'On-site' => t('Can be used on-site only'),
    'Not available' => t('Not available'),
    'Unknown' => t('No information is provided'),
  );
  return $values;
}